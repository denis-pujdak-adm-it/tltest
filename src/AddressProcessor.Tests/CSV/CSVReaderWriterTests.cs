﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AddressProcessing.CSV;
using NUnit.Framework;
using static AddressProcessing.CSV.CSVReaderWriter;

namespace Csv.Tests
{
    [TestFixture]
    public class CSVReaderWriterTests
    {
        const string TestFileName = @"test_data\test.csv";
        private CSVReaderWriter GlobalCSVReaderWriter;

        #region Setup

        [SetUp]
        public void Init()
        {
            GlobalCSVReaderWriter = new CSVReaderWriter();
        }

        [TearDown]
        public void Dispose()
        {
            // Dispose global stream
            if (GlobalCSVReaderWriter != null)
                GlobalCSVReaderWriter.Dispose();

            // Delete test file
            var testFile = new FileInfo(TestFileName);
            if (testFile.Exists)
                testFile.Delete();
        }

        #endregion Setup

        #region Tests

        [Test]
        public void Can_Write_To_CSV_File()
        {
            // Write data
            using (var csvWriter = new CSVReaderWriter(TestFileName, Mode.Write))
            {
                csvWriter.WriteRow(new[] { "abc1", "def1" });
                csvWriter.WriteRow(new[] { "abc2", "def2" });
                csvWriter.WriteRow(new[] { "abc3", "def3" });
            }

            // Read data
            var result = new List<string[]>();
            using (var csvReader = new CSVReaderWriter(TestFileName, Mode.Read))
            {
                string[] row;
                while (csvReader.ReadRow(out row))
                    result.Add(row);
            }

            // Check
            Assert_Data_Are_Correct(result);
        }

        [Test]
        public void Can_Write_To_CSV_File_Using_Deprecated_Methods()
        {
            var result = new List<string[]>();
            using (var csvReaderWriter = new CSVReaderWriter())
            {
                // Write data
                csvReaderWriter.Open(TestFileName, Mode.Write);

                csvReaderWriter.Write(new[] { "abc1", "def1" });
                csvReaderWriter.Write(new[] { "abc2", "def2" });
                csvReaderWriter.Write(new[] { "abc3", "def3" });
                csvReaderWriter.Close();

                // Read data
                csvReaderWriter.Open(TestFileName, Mode.Read);
                string column1, column2;
                while (csvReaderWriter.Read(out column1, out column2))
                    result.Add(new string[] { column1, column2 });
            }
            // Check
            Assert_Data_Are_Correct(result);
        }

        [Test]
        [TestCase(Mode.Read)]
        [TestCase(Mode.Write)]
        [ExpectedException(ExpectedMessage = "is already opened. Close existing stream before you can open it again", MatchType = MessageMatch.Contains)]
        public void Cannot_Use_Open_Simultaneously(Mode mode)
        {
            Create_Empty_Test_File_If_DoesNot_Exist();
            GlobalCSVReaderWriter.Open(TestFileName, Flip_Read_Write_Mode(mode));
            GlobalCSVReaderWriter.Open(TestFileName, mode);
        }

        [Test]
        [TestCase(Mode.Read)]
        [TestCase(Mode.Write)]
        [ExpectedException(ExpectedMessage = "is already opened. Close existing stream before you can open it again", MatchType = MessageMatch.Contains)]
        public void Cannot_Open_Multiple_Times(Mode mode)
        {
            Create_Empty_Test_File_If_DoesNot_Exist();
            GlobalCSVReaderWriter.Open(TestFileName, mode);
            GlobalCSVReaderWriter.Open(TestFileName, mode);
        }

        [Test]
        [ExpectedException(ExpectedMessage = "File 'test_data\\test.csv' does not exist", MatchType = MessageMatch.Contains)]
        public void When_File_Does_Not_Exist()
        {
            GlobalCSVReaderWriter.Open(TestFileName, Mode.Read);
        }

        [Test]
        [ExpectedException(ExpectedMessage = "The file mode '0' not implemented", MatchType = MessageMatch.Contains)]
        public void When_Mode_Has_Many_Flags()
        {
            GlobalCSVReaderWriter.Open(TestFileName, Mode.Read & Mode.Write);
        }

        [Test]
        [ExpectedException(ExpectedMessage = "is not in Read mode", MatchType = MessageMatch.Contains)]
        public void When_Stream_Has_Not_Been_Opened_When_Read()
        {
            string[] columns;
            GlobalCSVReaderWriter.ReadRow(out columns);
        }

        [Test]
        [ExpectedException(ExpectedMessage = "is not in Write mode", MatchType = MessageMatch.Contains)]
        public void When_Stream_Has_Not_Been_Opened_When_Write()
        {
            GlobalCSVReaderWriter.WriteRow("abc", "def");
        }

        [Test]
        [TestCase(Mode.Read)]
        [TestCase(Mode.Write)]
        [ExpectedException(ExpectedMessage = "has been disposed or not created yet", MatchType = MessageMatch.Contains)]
        public void When_Stream_Has_Already_Been_Closed(Mode mode)
        {
            Create_Empty_Test_File_If_DoesNot_Exist();
            GlobalCSVReaderWriter.Open(TestFileName, mode);
            GlobalCSVReaderWriter.Close();

            if (mode == Mode.Read)
            {
                string[] columns;
                GlobalCSVReaderWriter.ReadRow(out columns);
            }
            else
            {
                GlobalCSVReaderWriter.WriteRow("abc", "def");
            }
        }

        [Test]
        [ExpectedException(ExpectedMessage = "is not in Write mode", MatchType = MessageMatch.Contains)]
        public void When_We_Write_In_Read_Mode()
        {
            Create_Empty_Test_File_If_DoesNot_Exist();
            GlobalCSVReaderWriter.Open(TestFileName, Mode.Read);
            GlobalCSVReaderWriter.WriteRow("abc", "def");
        }

        [Test]
        [ExpectedException(ExpectedMessage = "is not in Read mode", MatchType = MessageMatch.Contains)]
        public void When_We_Read_In_Write_Mode()
        {
            GlobalCSVReaderWriter.Open(TestFileName, Mode.Write);
            string[] columns;
            GlobalCSVReaderWriter.ReadRow(out columns);
        }

        #endregion Tests

        #region Private Methods

        private void Assert_Data_Are_Correct(List<string[]> result)
        {
            Assert.AreEqual(3, result.Count, "Number of rows is different.");
            Assert.IsNotNull(result[0], "Column data are not present.");
            Assert.AreEqual(2, result[0].Length, "Number of columns is different.");
            Assert.AreEqual("abc1", result[0][0], "Column data are different.");
            Assert.AreEqual("def1", result[0][1], "Column data are different.");
            Assert.AreEqual("abc2", result[1][0], "Column data are different.");
            Assert.AreEqual("def3", result[2][1], "Column data are different.");
        }

        private Mode Flip_Read_Write_Mode(Mode mode)
        {
            return mode == Mode.Read ? Mode.Write : Mode.Read;
        }

        private void Create_Empty_Test_File_If_DoesNot_Exist()
        {
            var testFile = new FileInfo(TestFileName);
            if (!testFile.Exists)
                testFile.Create().Close();
        }

        #endregion Private Methods
    }
}
