﻿using System;
using System.IO;

namespace AddressProcessing.CSV
{
    /*
        1) List three to five key concerns with this implementation that you would discuss with the junior developer. 

        Please leave the rest of this file as it is so we can discuss your concerns during the next stage of the interview process.
        
        *)
        *)
        *)
    */

    /*
     
        1. Missing IDisposable implementation
        2. When I create a CSVReaderWriterForAnnotation and use it I have no information which "Mode" am I supossed to use. There gonna be bang if I Open in Read mode and then try to Write.
        3. Instead of having two methods to run (Open->Write or Open->Read) I'd rather put Open into constructor or split the class into two classes (CSVReader and CSVWriter).
        4. Danger!!! The method Open() can be accidentally used more than one time so streams could not be closed but overriden which will cause a memory leak.
        5. The code "for (int i = 0; i < columns.Length; i++) outPut += columns[i]; ..." is too long and not readable. We can use "string.Join()" instead.
        6. The line "_writerStream = fileInfo.CreateText();" should we check whether file already exist?
        7. The line "throw new Exception("Unknown file mode for " + fileName);" I would like to include enum Mode value into message as it will speed up an investigation process and chage from Exception to NotImplementedException.
        8. The line "char[] separator = { '\t' };". Separator should be defined at least as a static or constant field on the top of class and used everywhere inside a class. (At least).
        9. The line "public bool Read(string column1, string column2)" you should use "ref" if you want to use strings as a references inside the method.
        10. The line "public bool Read(string column1, string column2)" if the class supossed to read only two columns should it write two columns too? Otherwise the methods Read, Write are not fully compatible between each other.
        11. The method "private string ReadLine() { return _readerStream.ReadLine(); }". I would use the check "EndOfStream". I am not sure but it might throw an IO exception when we reached the end of the file.
        12. The method names "Write()" and "Read()" should be called "WriteRow()" and "ReadRow()" as what exactly they do.
        13. If we want rather to keep "Open()" method we should add a validation to "private void WriteLine(string line)" and "private string ReadLine()" whether streams are created/opened.
        14. For better memory management we might want to add a "Flush" method or just add Flush after we write a line. E.g. we might want to Flush after every line added or after N lines added (internal auto flush with counter or AutoFlush).
        15. FIRST_COLUMN, SECOND_COLUMN can be declared on the top of the class ones.
        16. If we want to keep two methods Read() we can use one method inside another two prevent repeating the same logic two times. Eg. public bool Read(ref string, ref string) { return Read(out string, out string); }.
        17. The method Read(string column1, ...) after the method has been executed the values column1 and column2 will stay the same without changes (see pkt. 11 above).
        18. The code "column1 = null" repeated twice in the method "Read(out string column1, ....)".
        19. The Flags attribute should be removed as it will allow for multiple enum options and create a chaos if somebody pases two options as a parameter of the Open() method.
        20. As classes purpose is to read CSV files we might need to add a validation whether it CSV extention or not (nevermind :-)

    */

    public class CSVReaderWriterForAnnotation
    {
        private StreamReader _readerStream = null;
        private StreamWriter _writerStream = null;

        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        public void Open(string fileName, Mode mode)
        {
            if (mode == Mode.Read)
            {
                _readerStream = File.OpenText(fileName);
            }
            else if (mode == Mode.Write)
            {
                FileInfo fileInfo = new FileInfo(fileName);
                _writerStream = fileInfo.CreateText();
            }
            else
            {
                throw new Exception("Unknown file mode for " + fileName);
            }
        }

        public void Write(params string[] columns)
        {
            string outPut = "";

            for (int i = 0; i < columns.Length; i++)
            {
                outPut += columns[i];
                if ((columns.Length - 1) != i)
                {
                    outPut += "\t";
                }
            }

            WriteLine(outPut);
        }

        public bool Read(string column1, string column2)
        {
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            string line;
            string[] columns;

            char[] separator = { '\t' };

            line = ReadLine();
            columns = line.Split(separator);

            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            }
            else
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        public bool Read(out string column1, out string column2)
        {
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            string line;
            string[] columns;

            char[] separator = { '\t' };

            line = ReadLine();

            if (line == null)
            {
                column1 = null;
                column2 = null;

                return false;
            }

            columns = line.Split(separator);

            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            } 
            else
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        private void WriteLine(string line)
        {
            _writerStream.WriteLine(line);
        }

        private string ReadLine()
        {
            return _readerStream.ReadLine();
        }

        public void Close()
        {
            if (_writerStream != null)
            {
                _writerStream.Close();
            }

            if (_readerStream != null)
            {
                _readerStream.Close();
            }
        }
    }
}
