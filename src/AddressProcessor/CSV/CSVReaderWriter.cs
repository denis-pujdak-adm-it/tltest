﻿using System;
using System.IO;
using System.Text;

namespace AddressProcessing.CSV
{
    /*
        2) Refactor this class into clean, elegant, rock-solid & well performing code, without over-engineering.
           Assume this code is in production and backwards compatibility must be maintained.

        Denis: I have added two additional methods and used them in the old methods to keep compatibility with backwards
    */

    /// <summary>
    /// Reads and writes CSV file
    /// </summary>
    public class CSVReaderWriter : IDisposable
    {
        public enum Mode { Read = 1, Write = 2 };

        const char CSV_COLUMN_SEPARATOR = '\t';

        // Column positions (zero based index)
        const int FIRST_COLUMN = 0;
        const int SECOND_COLUMN = 1;

        private StreamReader _readerStream = null;
        private StreamWriter _writerStream = null;

        private Mode _mode;

        [Obsolete("Do not use this anymore")]
        public CSVReaderWriter()
        {
        }

        public CSVReaderWriter(string fileName, Mode mode)
        {
            Open(fileName, mode);
        }

        #region Public Methods

        [Obsolete("Do not use this anymore. Use parametrized constractor instead.")]
        /// <summary>
        /// Opens a CSV file for reading or writing
        /// </summary>
        /// <param name="fileName">Full path name</param>
        /// <param name="mode">Read or Write mode</param>
        public void Open(string fileName, Mode mode)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentNullException("fileName");

            if (_readerStream != null)
                throw new Exception("StreamReader is already opened. Close existing stream before you can open it again.");

            if (_writerStream != null)
                throw new Exception("StreamWriter is already opened. Close existing stream before you can open it again.");

            if (mode == Mode.Read)
            {
                if (!new FileInfo(fileName).Exists)
                    throw new Exception(string.Format("File '{0}' does not exist", fileName));

                _readerStream = new StreamReader(fileName);
            }
            else if (mode == Mode.Write)
            {
                _writerStream = new StreamWriter(fileName);
                _writerStream.AutoFlush = true; // Memory optimization
            }
            else
            {
                throw new NotImplementedException(string.Format("The file mode '{0}' not implemented.", mode));
            }
            _mode = mode;
        }

        /// <summary>
        /// Writes a csv row into file
        /// </summary>
        /// <param name="columns">List of column values</param>
        public void WriteRow(params string[] columns)
        {
            string outPut = string.Join(CSV_COLUMN_SEPARATOR.ToString(), columns);
            WriteLine(outPut);
        }

        /// <summary>
        /// Reads all columns of the row from CSV file.
        /// </summary>
        /// <param name="columns">List of column values output</param>
        /// <returns>Returns false if stream reaches the end of the file</returns>
        public bool ReadRow(out string[] columns)
        {
            string line;

            if (ReadLine(out line))
            {
                if (line != null)
                    columns = line.Split(new char[] { CSV_COLUMN_SEPARATOR });
                else
                    columns = new string[0];

                return true;
            }
            else
            {
                columns = new string[0];
                return false;
            }
        }

        #endregion Public Methods

        #region Private methods

        /// <summary>
        /// Writes line into the file
        /// </summary>
        /// <param name="line">String line</param>
        private void WriteLine(string line)
        {
            if (_writerStream == null && _mode == Mode.Write)
                throw new Exception("StreamWriter has been disposed or not created yet.");

            if (_mode == Mode.Write)
            {
                _writerStream.WriteLine(line);
            }
            else
            {
                throw new Exception("The CSVReaderWriter is not in Write mode.");
            }
        }

        /// <summary>
        /// Reads line from the file.
        /// </summary>
        /// <param name="result">Returns a line</param>
        /// <returns>Returns false if stream reaches the end of the file.</returns>
        private bool ReadLine(out string result)
        {
            if (_readerStream == null && _mode == Mode.Read)
                throw new Exception("StreamReader has been disposed or not created yet.");

            if (_mode == Mode.Read)
            {
                if (_readerStream.EndOfStream)
                {
                    result = null;
                    return false;
                }
                else
                {
                    result = _readerStream.ReadLine();
                    return true;
                }
            }
            else
            {
                throw new Exception("The CSVReaderWriter is not in Read mode.");
            }
        }

        #endregion Private Methods

        #region IDisposable

        public void Dispose()
        {
            if (_writerStream != null)
            {
                _writerStream.Close();
                _writerStream = null;
            }
            if (_readerStream != null)
            {
                _readerStream.Close();
                _readerStream = null;
            }
        }

        #endregion IDisposable

        #region Obsolete Stuff

        // Obsolete stuff. Get rid of it as soon as posible.

        [Obsolete("Do not use this method anymore. Use WriteRow instead.")]
        public void Write(params string[] columns)
        {
            WriteRow(columns);
        }

        [Obsolete("Specific example only. Use ReadRow to fetch all columns instead.")]
        /// <summary>
        /// Reads all columns of the row from CSV file.
        /// </summary>
        /// <param name="columns">List of column values output</param>
        /// <returns>Returns false if stream reaches the end of the file</returns>
        public bool Read(out string column1, out string column2)
        {
            string[] columns;

            var result = ReadRow(out columns);

            column1 = result && columns.Length > FIRST_COLUMN ? columns[FIRST_COLUMN] : null;
            column2 = result && columns.Length > SECOND_COLUMN ? columns[SECOND_COLUMN] : null;

            return result;
        }

        [Obsolete("Do not use this method anymore. Use IDisposable instead.")]
        /// <summary>
        /// Closes file stream. You can open the file again after that.
        /// </summary>
        public void Close()
        {
            Dispose();
        }

        #endregion Obsolete Stuff
    }
}
